export enum UserRole {
  superuser = 'superuser',
  administrator = 'administrator',
  expert = 'expert',
  moderator = 'moderator',
  user = 'user',
}

export type UserRoles = UserRole[];

export interface User {
  id: string;
  first_name: string;
  last_name: string;
  middle_name: string;
  email: string;
  company_id: number;
  ideas_count: number;
  job_id: number;
  phone: string;
  photo: string | null;
  work_experience: number;
  role: UserRoles;
  rating: number;
  readonly significantRole: UserRole;
  readonly significantRoleText: string | void;
}

export class UserModel implements User {
  public id: string;
  public first_name: string;
  public last_name: string;
  public middle_name: string;
  public email: string;
  public company_id: number;
  public ideas_count: number;
  public job_id: number;
  public phone: string;
  public photo: string | null;
  public work_experience: number;
  public role: UserRoles;
  public rating: number;

  public static getRoleTextByRole(role: UserRole): string | void {
    if (role === UserRole.expert) return 'Эксперт';
    if (role === UserRole.administrator) return 'Администратор';
    if (role === UserRole.moderator) return 'Модератор';
    if (role === UserRole.user) return 'Участник';
    if (role === UserRole.superuser) return 'Супер-юзер';
  }

  public static getRoleByRoleText(text: string): string | void {
    if (text === 'Эксперт') return UserRole.expert;
    if (text === 'Администратор') return UserRole.administrator;
    if (text === 'Модератор') return UserRole.moderator;
    if (text === 'Участник') return UserRole.user;
    if (text === 'Супер-юзер') return UserRole.superuser;
  }

  constructor(model: User) {
    this.id = model.id;
    this.first_name = model.first_name;
    this.last_name = model.last_name;
    this.middle_name = model.middle_name;
    this.email = model.email;
    this.company_id = model.company_id;
    this.ideas_count = model.ideas_count || 0;
    this.job_id = model.job_id;
    this.phone = model.phone;
    this.photo = model.photo || null;
    this.work_experience = model.work_experience;
    this.role = model.role;
    this.rating = model.rating || 0;
  }

  get significantRole() {
    if (this.role.includes(UserRole.superuser)) return UserRole.superuser;
    if (this.role.includes(UserRole.administrator)) return UserRole.administrator;
    if (this.role.includes(UserRole.moderator)) return UserRole.moderator;
    if (this.role.includes(UserRole.expert)) return UserRole.expert;

    return UserRole.user;
  }

  get significantRoleText() {
    return UserModel.getRoleTextByRole(this.significantRole);
  }
}
