export enum RegistrationStatusOptions {
  on = 'on',
  off = 'off',
}

export interface RegistrationStatus {
  registration: RegistrationStatusOptions;
}

export interface RegistrationStatusInternal extends RegistrationStatus {
  readonly available: boolean;
}

export class RegistrationStatusModel implements RegistrationStatusInternal {
  public registration: RegistrationStatusOptions;

  constructor(model: RegistrationStatus | RegistrationStatusInternal) {
    this.registration = model.registration;
  }

  get available() {
    return this.registration === RegistrationStatusOptions.on;
  }
}
