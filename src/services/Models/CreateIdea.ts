export interface CreateIdea {
  text: string;
  effect: string;
  process_stage: string;
  source: string;
  category_id: number | string;
  experts: number[];
  files: number[];
}

export class CreateIdeaModel implements CreateIdea {
  public text: string;
  public effect: string;
  public process_stage: string;
  public source: string;
  public category_id: number | string;
  public experts: number[];
  public files: number[];

  constructor(model: CreateIdea) {
    this.text = model.text;
    this.effect = model.effect;
    this.process_stage = model.process_stage;
    this.source = model.source;
    this.category_id = model.category_id;
    this.experts = model.experts.map(Number);
    this.files = model.files.map(Number);
  }
}
