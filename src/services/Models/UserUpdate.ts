import {UserRoles} from '@/services/Models/User';

export interface UserUpdate {
  first_name?: string;
  last_name?: string;
  middle_name?: string;
  email?: string;
  company_id?: number;
  job_id?: number;
  phone?: string;
  photo?: string | null;
  work_experience?: number;
  role?: UserRoles;
}

export class UserUpdateModel implements UserUpdate {
  public first_name?: string;
  public last_name?: string;
  public middle_name?: string;
  public email?: string;
  public company_id?: number;
  public job_id?: number;
  public phone?: string;
  public photo?: string | null;
  public work_experience?: number;
  public role?: UserRoles;

  constructor(model: UserUpdate) {
    if ('first_name' in model) {
      this.first_name = model.first_name;
    }

    if ('last_name' in model) {
      this.last_name = model.last_name;
    }

    if ('middle_name' in model) {
      this.middle_name = model.middle_name;
    }

    if ('email' in model) {
      this.email = model.email;
    }

    if ('company_id' in model) {
      this.company_id = model.company_id;
    }

    if ('job_id' in model) {
      this.job_id = model.job_id;
    }

    if ('phone' in model) {
      this.phone = model.phone;
    }

    if ('photo' in model) {
      this.photo = model.photo || null;
    }

    if ('work_experience' in model) {
      this.work_experience = model.work_experience;
    }

    if ('role' in model) {
      this.role = model.role;
    }
  }
}
