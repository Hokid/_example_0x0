export interface File {
  id: number;
  file_name: string;
  file_url: string;
  size: number;
}

export class FileModel implements File {
  public id: number;
  public file_name: string;
  public file_url: string;
  public size: number;

  constructor(model: File) {
    this.id = model.id;
    this.file_name = model.file_name;
    this.file_url = model.file_url;
    this.size = model.size;
  }
}
