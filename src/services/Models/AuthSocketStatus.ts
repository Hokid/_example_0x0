export interface AuthSocketStatus {
  status: string;
}

export class AuthSocketStatusModel implements AuthSocketStatus {
  public static STATUS_SUCCESS = 'success';
  public static STATUS_FAILURE = 'failure';

  public status: string;

  constructor(model: AuthSocketStatus) {
    this.status = status;
  }
}
