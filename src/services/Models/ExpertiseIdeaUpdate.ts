export interface ExpertiseIdeaUpdate {
  comment: string;
  relevance: number;
  implementability: number;
  potential_value: number;
  costs: number;
}

export class ExpertiseIdeaUpdateModel implements ExpertiseIdeaUpdate {
  public comment: string;
  public relevance: number;
  public implementability: number;
  public potential_value: number;
  public costs: number;

  constructor(model: ExpertiseIdeaUpdate) {
    this.comment = model.comment;
    this.relevance = model.relevance;
    this.implementability = model.implementability;
    this.potential_value = model.potential_value;
    this.costs = model.costs;
  }
}
