export interface ResetPasswordStep2 {
  token: string;
  password: string;
  password2: string;
}

export class ResetPasswordStep2Model implements ResetPasswordStep2 {
  public token: string;
  public password: string;
  public password2: string;

  constructor(model: ResetPasswordStep2) {
    this.token = model.token;
    this.password = model.password;
    this.password2 = model.password2;
  }
}
