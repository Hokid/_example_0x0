import {IdeaCategory, IdeaCategoryModel} from '@/services/Models/IdeaCategory';

export interface Expert {
  id: number;
  first_name: string;
  last_name: string;
  description: string;
  photo: string;
  expertise: IdeaCategory[];
}

export class ExpertModel implements Expert {
  public id: number;
  public first_name: string;
  public last_name: string;
  public description: string;
  public photo: string;
  public expertise: IdeaCategory[];

  constructor(model: Expert) {
    this.id = model.id;
    this.first_name = model.first_name;
    this.last_name = model.last_name;
    this.description = model.description;
    this.photo = model.photo;
    this.expertise = (model.expertise || []).map((item) => new IdeaCategoryModel(item));
  }
}
