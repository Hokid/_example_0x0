export interface Company {
  id: number;
  name: string;
}

export class CompanyModel implements Company {
  public id: number;
  public name: string;

  constructor(model: Company) {
    this.id = model.id;
    this.name = model.name;
  }
}
