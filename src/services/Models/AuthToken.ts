export interface AuthToken {
  token: string;
}

export class AuthTokenModel implements AuthToken {
  public token: string;

  constructor(model: AuthToken) {
    this.token = model.token;
  }
}
