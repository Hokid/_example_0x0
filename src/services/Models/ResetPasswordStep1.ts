export interface ResetPasswordStep1 {
  email: string;
}

export class ResetPasswordStep1Model implements ResetPasswordStep1 {
  public email: string;

  constructor(model: ResetPasswordStep1) {
    this.email = model.email;
  }
}
