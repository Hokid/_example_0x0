export const NO_CATEGORIES_ID = 0;
export const NO_CATEGORIES_NAME = 'Нет категории';

export interface IdeaCategory {
  id: number;
  name: string;
}

export class IdeaCategoryModel implements IdeaCategory {
  public id: number;
  public name: string;

  constructor(model: IdeaCategory) {
    this.id = model ? model.id : NO_CATEGORIES_ID;
    this.name = model ? model.name : NO_CATEGORIES_NAME;
  }
}

