export interface IdeaAuthor {
  id: number;
  first_name: string;
  last_name: string;
  middle_name: string;
  photo: string;
  company_id: number;
  job_id: number;
  work_experience: number;
}

export class IdeaAuthorModel implements IdeaAuthor {
  public id: number;
  public first_name: string;
  public last_name: string;
  public middle_name: string;
  public photo: string;
  public company_id: number;
  public job_id: number;
  public work_experience: number;

  constructor(model: IdeaAuthor) {
    this.id = model.id;
    this.first_name = model.first_name;
    this.last_name = model.last_name;
    this.middle_name = model.middle_name;
    this.photo = model.photo;
    this.company_id = model.company_id;
    this.job_id = model.job_id;
    this.work_experience = model.work_experience;
  }
}

