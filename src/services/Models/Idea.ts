import {File, FileModel} from './File';
import {IdeaCategory, IdeaCategoryModel} from '@/services/Models/IdeaCategory';
import {IdeaStatus, IdeaStatusModel} from '@/services/Models/IdeaStatus';
import {Expert, ExpertModel} from '@/services/Models/Expert';
import {IdeaAuthor, IdeaAuthorModel} from '@/services/Models/IdeaAuthor';
import {User, UserModel} from '@/services/Models/User';

export interface Idea {
  id: number;
  text: string;
  effect: string;
  process_stage: string;
  source: string;
  category: IdeaCategory;
  status_id: string;
  status: string;
  rating: number;
  created_at: string;
  modified_at: string;
  moderator_comment: string;
  expertise_done: boolean;
  moderator: User;
  experts: Expert[];
  files: File[];
  author: IdeaAuthor;
}

export interface IdeaInternal {
  id: number;
  text: string;
  effect: string;
  process_stage: string;
  source: string;
  category: IdeaCategory;
  status: IdeaStatus;
  rating: number;
  created_at: string;
  modified_at: string;
  moderator_comment: string;
  expertise_done: boolean;
  moderator: User | null;
  experts: Expert[];
  files: File[];
  author: IdeaAuthor | null;
}

export class IdeaModel implements IdeaInternal {
  id: number;
  text: string;
  effect: string;
  process_stage: string;
  source: string;
  category: IdeaCategory;
  status: IdeaStatus;
  rating: number;
  created_at: string;
  modified_at: string;
  moderator_comment: string;
  expertise_done: boolean;
  moderator: User | null;
  experts: Expert[];
  files: File[];
  author: IdeaAuthor | null;

  constructor(model: Idea | IdeaInternal) {
    this.id = model.id;
    this.text = model.text;
    this.effect = model.effect;
    this.process_stage = model.process_stage;
    this.source = model.source;
    this.category = new IdeaCategoryModel(model.category);
    this.status = (
      typeof model.status === 'object'
        ? new IdeaStatusModel((model as IdeaInternal).status)
        : new IdeaStatusModel({
          id: (model as Idea).status_id,
          name: (model as Idea).status
        })
    );;
    this.rating = model.rating;
    this.created_at = model.created_at;
    this.modified_at = model.modified_at;
    this.moderator_comment = model.moderator_comment;
    this.expertise_done = !!model.expertise_done;
    this.moderator = model.moderator ? new UserModel(model.moderator) : null;
    this.experts = model.experts ? model.experts.map((item) => new ExpertModel(item)) : [];
    this.files = model.files ? model.files.map((item) => new FileModel(item)) : [];
    this.author	= model.author ? new IdeaAuthorModel(model.author) : null;
  }
}
