import {NotificationMessage} from '@/services/Models/NotificationMessage';

export interface IdeaNotification extends NotificationMessage {
  type: 'idea';
  object_id: number;
  is_viewed: boolean;
  created_at: string;
}

export class IdeaNotificationModel implements IdeaNotification {
  public id: string;
  public title: string;
  public content: string;
  public type: 'idea';
  public object_id: number;
  public is_viewed: boolean;
  public created_at: string;

  constructor(model: IdeaNotification) {
    this.id = model.id;
    this.title = model.title;
    this.content = model.content;
    this.type = 'idea';
    this.object_id = model.object_id;
    this.is_viewed = model.is_viewed;
    this.created_at = model.created_at;
  }
}
