import axios from 'axios';

export default axios.create({
  // set the api url
  baseURL: process.env.VUE_APP_API_URL,
});
