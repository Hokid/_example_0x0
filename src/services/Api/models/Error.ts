const exclude = ['code', 'message', 'additional'];

export class ErrorModel {
  public code: number;
  public message: string;
  public additional: { [key: string]: string };

  constructor(error = {} as {
    code: number,
    message?: string,
    additional: { [key: string]: string[] | string },
    [key: string]: any,
  }) {
    this.code = error.code;
    this.message = error.message || '';
    this.additional = {};

    const additional = Object.assign({}, error.additional);

    Object.keys(error)
      .filter((key) => !exclude.includes(key))
      .forEach((key) => {
        additional[key] = error[key];
      });

    for (const key in additional) {
      if (typeof additional[key] !== 'string') {
        if (Array.isArray(additional[key])) {
          additional[key] = (additional[key] as string[]).join(', ');
        } else {
          additional[key] = JSON.stringify(additional[key]);
        }
      }
    }

    Object.assign(this.additional, additional as { [key: string]: string });
  }
}
