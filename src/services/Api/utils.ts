import { AxiosError, AxiosResponse } from 'axios';
import { ErrorModel } from './models/Error';
import {Pagination, PaginationModel} from '@/services/Models/Pagination';

export function transformResponseToErrorModel(
  error: AxiosError & { code: number, message: string },
  doThrow?: true,
): never;
export function transformResponseToErrorModel(
  error: AxiosError & { code: number, message: string },
  doThrow?: false,
): never;
export function transformResponseToErrorModel(
  error: AxiosError & { code: number, message: string },
  doThrow = true,
) {
  const model = {
    code: -200,
    message: 'error has no description',
    additional: {},
  };

  console.log(error);

  if (
    error.response
    &&
    error.response.data
  ) {
    const data = error.response.data;

    let message: string | string[] = data.errors as string[];
    let code = data.code as number;

    if (Array.isArray(message)) {
      message = message.join(', ');
    }

    if (message != null) {
      if (typeof message === 'object') {
        message = JSON.stringify(message);
      }

      delete data.errors;
    } else {
      message = error.response.statusText || model.message;
    }

    if (code == null) {
      code = error.response.status;
    }

    Object.assign(model, error.response.data);

    model.code = code as number;
    model.message = message as string;
  } else if (error.request) {
    model.code = error.request.status;
    model.message = error.request.statusText;
  } else if (
    typeof model.code === 'number'
    && typeof model.message === 'string'
  ) {
    model.code = error.code;
    model.message = error.message;
  }

  const response = new ErrorModel(model);

  if (doThrow) {
    throw response;
  }

  return response;
}

export function responseToModel<I, O>(Model: { new(model: I): O }, list?: false): (response: AxiosResponse<I>) => O;
export function responseToModel<I, O>(Model: { new(model: I): O }, list?: true): (response: AxiosResponse<I[]>) => O[];
export function responseToModel<I, O>(Model: { new(model: I): O }, list = false): (response: AxiosResponse<I | I[]>) => O | O[] {
  return (response: AxiosResponse<I | I[]>) => {
    if (!list) {
      return new Model(response.data as I);
    } else {
      return (response.data as I[]).map((item: I) => new Model(item));
    }
  };
}

export function responseToPaginationModel<I, O>(
  Model: { new(model: I): O }
): (response: AxiosResponse<Pagination<I>>) => Pagination<O> {
  return (response: AxiosResponse<Pagination<I>>) => {
    return new PaginationModel<I, O>(response.data, Model);
  };
}
