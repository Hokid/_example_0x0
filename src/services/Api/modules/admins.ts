import auth from '../Auth';
import base from '../Base';
import {transformResponseToErrorModel, responseToModel, responseToPaginationModel} from '@/services/Api/utils';
import {Idea, IdeaInternal, IdeaModel} from '@/services/Models/Idea';
import {IdeaUpdate, IdeaUpdateModel} from '@/services/Models/IdeaUpdate';
import {throwIfNotInstanse} from '@/services/utils';
import {AdminsIdeaFilters} from '@/services/Admins/vuex/modules/ideas';

import moment from 'moment';
import {User, UserModel, UserRole} from '@/services/Models/User';
import {
  RegistrationStatus,
  RegistrationStatusInternal,
  RegistrationStatusModel
} from '@/services/Models/RegistrationStatus';
import {UserUpdateModel} from '@/services/Models/UserUpdate';
import {Pagination} from '@/services/Models/Pagination';

export function getIdeas(filters: AdminsIdeaFilters, offset?: number, limit = 10) {
  return (
    auth.request({
      method: 'get',
      url: '/admins/ideas/',
      params: {
        search: filters.searchRequest ? filters.searchRequest : null,
        category_id: filters.selectedCategory ? filters.selectedCategory : null,
        status_id: filters.selectedStatus ? filters.selectedStatus : null,
        df: filters.dateRange[0] ? moment(filters.dateRange[0]).format('YYYY-MM-DD') : null,
        dt: filters.dateRange[1] ? moment(filters.dateRange[1]).format('YYYY-MM-DD') : null,
        offset,
        limit: offset != null && offset >= 0 ? limit: void 0
      },
    })
      .then(responseToPaginationModel<Idea, IdeaInternal>(IdeaModel))
      .catch(transformResponseToErrorModel)
  );
}

export function getIdea(ideaId: string | number): Promise<IdeaModel> {
  return (
    auth.request({
      method: 'get',
      url: `/admins/ideas/${ideaId}/`,
    })
      .then(responseToModel(IdeaModel))
      .catch(transformResponseToErrorModel)
  );
}

export function updateIdea(ideaId: string | number, requestModel: IdeaUpdateModel) {
  return (
    auth.request({
      method: 'put',
      url: `/admins/ideas/${ideaId}/`,
      data: requestModel,
    })
      .then(responseToModel<Idea, IdeaInternal>(IdeaModel))
      .catch(transformResponseToErrorModel)
  );
}

export function deleteIdea(ideaId: string | number) {
  return (
    auth.request({
      method: 'delete',
      url: `/admins/ideas/${ideaId}/`,
    })
      .catch(transformResponseToErrorModel)
  );
}

export function getUsers(search?: string, role?: UserRole) {
  return (
    auth.request({
      method: 'get',
      url: `/admins/users`,
      params: {
        search,
        role,
      },
    })
      .then(responseToPaginationModel<User, User>(UserModel))
      .catch(transformResponseToErrorModel)
  );
}

export function getUser(id: number | string) {
  return (
    auth.request({
      method: 'get',
      url: `/admins/users/${id}`,
    })
      .then(responseToModel<User, User>(UserModel))
      .catch(transformResponseToErrorModel)
  );
}

export function updateUser(id: number | string, model: UserUpdateModel) {
  return (
    auth.request({
      method: 'patch',
      url: `/admins/users/${id}`,
      data: model,
    })
      .then(responseToModel<User, User>(UserModel))
      .catch(transformResponseToErrorModel)
  );
}

export function getRegistrationStatus() {
  return (
    base.request({
      method: 'get',
      url: '/admins/users/registration',
    })
      .then(responseToModel<RegistrationStatus, RegistrationStatusInternal>(RegistrationStatusModel))
      .catch(transformResponseToErrorModel)
  );
}

export function updateRegistrationStatus(model: RegistrationStatusModel) {
  return (
    auth.request({
      method: 'patch',
      url: '/admins/users/registration',
      data: model,
    })
      .then(responseToModel<RegistrationStatus, RegistrationStatusInternal>(RegistrationStatusModel))
      .catch(transformResponseToErrorModel)
  );
}
