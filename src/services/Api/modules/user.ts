import auth from '../Auth';
import base from '../Base';
import { GetTokenByLoginAndPasswordRequest } from '../models/request/GetTokenByLoginAndPasswordRequest';
import { TokenResponseModel } from '../models/responses/TokenResponseModel';
import { throwIfNotInstanse } from '@/services/utils';
import {transformResponseToErrorModel, responseToModel, responseToPaginationModel} from '@/services/Api/utils';
import {AxiosError, AxiosResponse} from 'axios';
import {UserModel, User} from '@/services/Models/User';
import {ErrorModel} from '@/services/Api/models/Error';
import {Registration, RegistrationModel} from '@/services/Models/Registration';
import {AuthToken, AuthTokenModel} from '@/services/Models/AuthToken';
import {LoginModel} from '@/services/Models/Login';
import {CreateIdeaModel} from '@/services/Models/CreateIdea';
import {Idea, IdeaInternal, IdeaModel} from '@/services/Models/Idea';
import {RatingHistoryItem, RatingHistoryItemModel} from '@/services/Models/RatingHistoryItem';
import {ResetPasswordStep1, ResetPasswordStep1Model} from '@/services/Models/ResetPasswordStep1';
import {ResetPasswordStep2, ResetPasswordStep2Model} from '@/services/Models/ResetPasswordStep2';
import {UserUpdate, UserUpdateModel} from '@/services/Models/UserUpdate';
import {IdeaNotification, IdeaNotificationModel} from '@/services/Models/IdeaNotification';
import {RatingNotification, RatingNotificationModel} from '@/services/Models/RatingNotification';
import {IdeaUpdate, IdeaUpdateModel} from '@/services/Models/IdeaUpdate';
import {ModeratorIdeaFilters} from '@/services/Moderators/vuex/modules/ideas';
// import qs from 'qs';

export function getAuthTokenByLoginAndPassword(requestModel: LoginModel) {
  throwIfNotInstanse(requestModel, LoginModel);

  return (
    base.request({
      method: 'post',
      url: '/users/login/',
      data: requestModel,
    })
      .then(responseToModel<TokenResponseModel, TokenResponseModel>(TokenResponseModel))
      .catch(transformResponseToErrorModel)
  );
}

export function getAccount() {
  return (
    auth.request({
      method: 'get',
      url: '/users/current/',
    })
      .then(responseToModel(UserModel))
      .catch(transformResponseToErrorModel)
  );
}

export function logout() {
  return (
    auth.request({
      method: 'get',
      url: '/users/logout/',
    })
      .then(() => {})
      .catch(transformResponseToErrorModel)
  );
}

export function registration(requestModel: RegistrationModel) {
  throwIfNotInstanse(requestModel, RegistrationModel);

  return (
    base.request({
      method: 'post',
      url: '/users/registration/',
      data: requestModel,
    })
      .then(responseToModel(AuthTokenModel))
      .catch(transformResponseToErrorModel)
  );
}

export function createIdea(userId: string, requestModel: CreateIdeaModel) {
  throwIfNotInstanse(requestModel, CreateIdeaModel);

  return (
    auth.request<Idea>({
      method: 'post',
      url: `/users/${userId}/ideas/`,
      data: requestModel,
    })
      .then(responseToModel<Idea, IdeaInternal>(IdeaModel))
      .catch(transformResponseToErrorModel)
  );
}

export function updateIdea(userId: string, ideaId: number | string, requestModel: IdeaUpdate) {
  throwIfNotInstanse(requestModel, IdeaUpdateModel);

  return (
    auth.request({
      method: 'put',
      url: `/users/${userId}/ideas/${ideaId}/`,
      data: requestModel,
    })
      .then(responseToModel<Idea, IdeaInternal>(IdeaModel))
      .catch(transformResponseToErrorModel)
  );
}

export function getIdeas(userId: string | number, offset?: number, limit = 10) {
  return (
    auth.request({
      method: 'get',
      url: `/users/${userId}/ideas/`,
      params: {
        offset,
        limit: offset != null && offset >= 0 ? limit : void 0
      }
    })
      .then(responseToPaginationModel<Idea, IdeaInternal>(IdeaModel))
      .catch(transformResponseToErrorModel)
  );
}

export function getIdea(userId: string | number, ideaId: string | number): Promise<IdeaModel> {
  return (
    auth.request({
      method: 'get',
      url: `/users/${userId}/ideas/${ideaId}/`,
    })
      .then(responseToModel<Idea, IdeaInternal>(IdeaModel))
      .catch(transformResponseToErrorModel)
  );
}

export function getRating(userId: string | number): Promise<number> {
  return (
    auth.request({
      method: 'get',
      url: `/users/${userId}/rating/`,
    })
      .then((response: AxiosResponse) => parseFloat(response.data))
      .catch(transformResponseToErrorModel)
  );
}

export function getRatingHistory(userId: string | number): Promise<RatingHistoryItem[]> {
  return (
    auth.request({
      method: 'get',
      url: `/users/${userId}/rating/history/`,
    })
      .then(responseToModel<RatingHistoryItem, RatingHistoryItem>(RatingHistoryItemModel, true))
      .catch(transformResponseToErrorModel)
  );
}

export function resetPasswordStep1(requestModel: ResetPasswordStep1): Promise<any> {
  throwIfNotInstanse(requestModel, ResetPasswordStep1Model);

  return (
    base.request({
      method: 'post',
      url: '/users/reset-password/',
      data: requestModel,
    })
      .catch(transformResponseToErrorModel)
  );
}

export function resetPasswordStep2(requestModel: ResetPasswordStep2): Promise<any> {
  throwIfNotInstanse(requestModel, ResetPasswordStep2Model);

  return (
    base.request({
      method: 'post',
      url: '/users/reset-password-step2/',
      data: requestModel,
    })
      .catch(transformResponseToErrorModel)
  );
}

export function updateUser(userId: string, requestModel: UserUpdate) {
  throwIfNotInstanse(requestModel, UserUpdateModel);

  return (
    auth.request({
      method: 'patch',
      url: `/users/${userId}/`,
      data: requestModel,
    })
      .then(responseToModel<User, User>(UserModel))
      .catch(transformResponseToErrorModel)
  );
}

export function getNotifications(userId: string) {
  return (
    auth.request({
      method: 'get',
      url: `/users/${userId}/notifications/`,
      params: {
        viewed: false,
      },
    })
      .then((response: AxiosResponse) => {
        const list = response.data;

        return list
          .map((item: IdeaNotification | RatingNotification) => {
            if (item.type === 'idea') {
              return new IdeaNotificationModel(item as IdeaNotification);
            } else if (item.type === 'rating') {
              return new RatingNotificationModel(item as RatingNotification);
            }
          })
          .filter(Boolean);
      })
      .catch(transformResponseToErrorModel)
  );
}

export function markNotificationAsViewed(userId: string, notificationId: string) {
  return (
    auth.request({
      method: 'patch',
      url: `/users/${userId}/notifications/${notificationId}/`,
      params: {
        is_viewed: true,
      },
    })
      .then((response: AxiosResponse) => {
        return true;
      })
      .catch(transformResponseToErrorModel)
  );
}
