import auth from '../Auth';
import {transformResponseToErrorModel, responseToModel, responseToPaginationModel} from '@/services/Api/utils';
import {Idea, IdeaInternal, IdeaModel} from '@/services/Models/Idea';
import {throwIfNotInstanse} from '@/services/utils';
import {ModeratorIdeaFilters} from '@/services/Moderators/vuex/modules/ideas';
import {ModeratorIdeaUpdate, ModeratorIdeaUpdateModel} from '@/services/Models/ModeratorIdeaUpdate';

import moment from 'moment';

export function getIdeas(filters: ModeratorIdeaFilters, offset?: number, limit = 10) {
  return (
    auth.request({
      method: 'get',
      url: `/moderators/ideas/`,
      params: {
        category_id: filters.selectedCategory ? filters.selectedCategory : null,
        df: filters.dateRange[0] ? moment(filters.dateRange[0]).format('YYYY-MM-DD') : null,
        dt: filters.dateRange[1] ? moment(filters.dateRange[1]).format('YYYY-MM-DD') : null,
        offset,
        limit: offset != null && offset >= 0 ? limit : void 0
      },
    })
      .then(responseToPaginationModel<Idea, IdeaInternal>(IdeaModel))
      .catch(transformResponseToErrorModel)
  );
}

export function getIdea(ideaId: string | number) {
  return (
    auth.request({
      method: 'get',
      url: `/moderators/ideas/${ideaId}/`,
    })
      .then(responseToModel<Idea, IdeaInternal>(IdeaModel))
      .catch(transformResponseToErrorModel)
  );
}

export function updateIdea(ideaId: string | number, requestModel: ModeratorIdeaUpdate) {
  throwIfNotInstanse(requestModel, ModeratorIdeaUpdateModel);

  return (
    auth.request({
      method: 'put',
      url: `/moderators/ideas/${ideaId}/`,
      data: requestModel,
    })
      .then(responseToModel<Idea, IdeaInternal>(IdeaModel))
      .catch(transformResponseToErrorModel)
  );
}
