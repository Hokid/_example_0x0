export function throwIfNotInstanse(instance: any, constructor: any) {
  if (!(instance instanceof constructor)) {
    throw new TypeError('object not instance of ' + constructor.name);
  }
}

export function byId<T>(list: Array<T & { id: number | string }>, id: number | string): T | void {
  return list.find((i) => i.id === id);
}
