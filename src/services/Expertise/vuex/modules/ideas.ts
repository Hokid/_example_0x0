import {ActionContext, Module} from 'vuex';
import {getIdea, getIdeas, updateIdea} from '@/services/Api/modules/expertise';
import {Idea, IdeaInternal} from '@/services/Models/Idea';
import {ExpertiseIdeaUpdateModel} from '@/services/Models/ExpertiseIdeaUpdate';
import {RootState} from '@/store/types';
import cloneDeep from 'lodash/cloneDeep';
import {AdminsIdeaFilters} from '@/services/Admins/vuex/modules/ideas';

export const RESET = 'RESET';
export const RESET_FILTERS = 'RESET_FILTERS';
export const UPDATE = 'UPDATE';
export const UPDATE_IDEA = 'UPDATE_IDEA';
export const UPDATE_FILTERS = 'UPDATE_FILTERS';
export const BULK_UPDATE = 'BULK_UPDATE';
export const UPDATE_META = 'UPDATE_META';

export interface StateMeta {
  loading: boolean;
  loadingError: boolean;
  completedLoading: boolean;
  offset: number;
  limit: number;
  total: number;
}

export interface ExpertiseIdeaFilters {
  selectedCategory: string | number | null;
  dateRange: Date[];
}
export interface State {
  meta: StateMeta;
  list: IdeaInternal[];
  filters: ExpertiseIdeaFilters;
}

const module: Module<State, RootState> = {
  namespaced: true,
  state: {
    meta: {
      loading: false,
      loadingError: false,
      // list full loaded indicator
      completedLoading: false,
      offset: 0,
      limit: 10,
      total: 0,
    },
    filters: {
      selectedCategory: null,
      dateRange: [],
    },
    list: [],
  },
  mutations: {
    [UPDATE_META](state: State, payload: StateMeta) {
      Object.assign(state.meta, payload);
    },
    [UPDATE](state: State, payload: State) {
      const list = payload.list;

      Object.assign(state, {list});
    },
    [UPDATE_IDEA](state: State, item: IdeaInternal) {
      const list = state.list;
      const foundIndex = list.findIndex((idea) => idea.id === item.id);

      if (foundIndex >= 0) {
        list.splice(foundIndex, 1, item);
      } else {
        list.unshift(item);
      }
    },
    [BULK_UPDATE](state: State, items: IdeaInternal[]) {
      for (const item of items) {
        const list = state.list;
        const foundIndex = list.findIndex((idea) => idea.id === item.id);

        if (foundIndex >= 0) {
          list.splice(foundIndex, 1, item);
        } else {
          list.push(item);
        }
      }
    },
    [UPDATE_FILTERS](state: State, payload: AdminsIdeaFilters) {
      state.filters = cloneDeep(payload);
    },
    [RESET](state: State) {
      state.meta.loading = false;
      state.meta.loadingError = false;
      state.meta.completedLoading = false;
      state.meta.offset = 0;
      state.meta.limit = 10;
      state.meta.total = 0;
      state.list = [];
    },
    [RESET_FILTERS](state: State) {
      state.filters = {
        selectedCategory: null,
        dateRange: []
      };
    },
  },
  actions: {
    async fetchAll({commit, state}: ActionContext<State, RootState>) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const {count, results} = await getIdeas(state.filters);

        commit(UPDATE, {list: results});
        commit(UPDATE_META, {
          loading: false,
          loadingError: false,
          completedLoading: true,
          offset: count - state.meta.limit,
          total: count,
        });

        return results;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async fetchById({commit}: ActionContext<State, RootState>, ideaId: string | number) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const idea = await getIdea(ideaId);

        commit(UPDATE_IDEA, idea);
        commit(UPDATE_META, {loading: false, loadingError: false});

        return idea;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async updateById(
      {commit, dispatch}: ActionContext<State, RootState>,
      {ideaId, data}: { ideaId: string | number, data: ExpertiseIdeaUpdateModel },
    ) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        await updateIdea(ideaId, data);
        // TODO: temporally solution. we must use response from updateIdea to update idea
        const idea = await dispatch('fetchById', ideaId);

        commit(UPDATE_META, {loading: false, loadingError: false});

        return idea;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async setFilters({commit}: ActionContext<State, RootState>, filters: ExpertiseIdeaFilters) {
      commit(UPDATE_FILTERS, filters);
      commit(RESET);
    },
    async fetchNext({commit, state}: ActionContext<State, RootState>) {
      if (state.meta.completedLoading) return [];

      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        let {limit, offset} = state.meta;
        const condition = offset + limit;

        if (state.list.length >= condition) {
          offset += limit;
        }

        const response = await getIdeas(state.filters, offset, limit);
        const {results, count} = response;

        commit(BULK_UPDATE, results);
        commit(UPDATE_META, {
          loading: false,
          loadingError: false,
          completedLoading: state.list.length >= count,
          offset,
          total: count,
        });

        return results;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async resetAll({commit}: ActionContext<State, RootState>) {
      commit(RESET);
      commit(RESET_FILTERS);
    },
  },
};

export default module;
