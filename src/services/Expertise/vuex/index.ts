import ideas from './modules/ideas';
import {Module} from 'vuex';
import {RootState} from '@/store/types';

const module: Module<{}, RootState> = {
  namespaced: true,
  state: {},
  modules: {
    ideas,
  },
  getters: {
  },
  mutations: {

  },
  actions: {

  },
};

export default module;
