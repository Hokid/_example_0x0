import {ActionContext, Module} from 'vuex';
import {getIdea, getIdeas, updateIdea, deleteIdea} from '@/services/Api/modules/admins';
import {Idea, IdeaInternal} from '@/services/Models/Idea';
import {RootState} from '@/store/types';
import {IdeaUpdate, IdeaUpdateModel} from '@/services/Models/IdeaUpdate';
import cloneDeep from 'lodash/cloneDeep';

export const RESET = 'RESET';
export const RESET_FILTERS = 'RESET_FILTERS';
export const UPDATE = 'UPDATE';
export const UPDATE_IDEA = 'UPDATE_IDEA';
export const DELETE_IDEA = 'DELETE_IDEA';
export const UPDATE_FILTERS = 'UPDATE_FILTERS';
export const BULK_UPDATE = 'BULK_UPDATE';
export const UPDATE_META = 'UPDATE_META';

export interface StateMeta {
  loading: boolean;
  loadingError: boolean;
  completedLoading: boolean;
  offset: number;
  limit: number;
  total: number;
}

export interface AdminsIdeaFilters {
  selectedCategory: string | number | null;
  dateRange: Date[];
  selectedStatus: string | number | null;
  searchRequest: string | null;
}

export interface State {
  meta: StateMeta;
  list: IdeaInternal[];
  filters: AdminsIdeaFilters;
}

const module: Module<State, RootState> = {
  namespaced: true,
  state: {
    meta: {
      loading: false,
      loadingError: false,
      // list full loaded indicator
      completedLoading: false,
      offset: 0,
      limit: 10,
      total: 0,
    },
    filters: {
      selectedCategory: null,
      dateRange: [],
      selectedStatus: null,
      searchRequest: null,
    },
    list: [],
  },
  mutations: {
    [UPDATE_META](state: State, payload: StateMeta) {
      Object.assign(state.meta, payload);
    },
    [UPDATE](state: State, payload: State) {
      const list = payload.list;

      Object.assign(state, {list});
    },
    [UPDATE_IDEA](state: State, item: IdeaInternal) {
      const list = state.list;
      const foundIndex = list.findIndex((idea) => idea.id === item.id);

      if (foundIndex >= 0) {
        list.splice(foundIndex, 1, item);
      } else {
        list.unshift(item);
      }
    },
    [DELETE_IDEA](state: State, id: number | string) {
      const list = state.list;
      const foundIndex = list.findIndex((idea) => idea.id == id);

      if (foundIndex >= 0) {
        list.splice(foundIndex, 1);
      }
    },
    [BULK_UPDATE](state: State, items: IdeaInternal[]) {
      for (const item of items) {
        const list = state.list;
        const foundIndex = list.findIndex((idea) => idea.id === item.id);

        if (foundIndex >= 0) {
          list.splice(foundIndex, 1, item);
        } else {
          list.push(item);
        }
      }
    },
    [UPDATE_FILTERS](state: State, payload: AdminsIdeaFilters) {
      state.filters = cloneDeep(payload);
    },
    [RESET](state: State) {
      state.meta.loading = false;
      state.meta.loadingError = false;
      state.meta.completedLoading = false;
      state.meta.offset = 0;
      state.meta.limit = 10;
      state.meta.total = 0;
      state.list = [];
    },
    [RESET_FILTERS](state: State) {
      state.filters = {
        selectedCategory: null,
        dateRange: [],
        selectedStatus: null,
        searchRequest: null,
      };
    },
  },
  actions: {
    async fetchAll({commit, state}: ActionContext<State, RootState>) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const {count, results} = await getIdeas(state.filters);

        commit(UPDATE, {list: results});
        commit(UPDATE_META, {
          loading: false,
          loadingError: false,
          completedLoading: true,
          offset: count - state.meta.limit,
          total: count,
        });

        return results;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async fetchById({commit}: ActionContext<State, RootState>, ideaId: string | number) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const idea = await getIdea(ideaId);

        commit(UPDATE_IDEA, idea);
        commit(UPDATE_META, {loading: false, loadingError: false});

        return idea;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async updateById(
      {commit}: ActionContext<State, RootState>,
      {ideaId, data}: { ideaId: string | number, data: IdeaUpdateModel },
    ) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const idea = await updateIdea(ideaId, data);

        commit(UPDATE_IDEA, idea);
        commit(UPDATE_META, {loading: false, loadingError: false});

        return idea;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async deleteById(
      {commit}: ActionContext<State, RootState>, ideaId: string | number) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        await deleteIdea(ideaId);

        commit(DELETE_IDEA, ideaId);
        commit(UPDATE_META, {loading: false, loadingError: false});
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async setFilters({commit}: ActionContext<State, RootState>, filters: AdminsIdeaFilters) {
      commit(UPDATE_FILTERS, filters);
      commit(RESET);
    },
    async fetchNext({commit, state}: ActionContext<State, RootState>) {
      if (state.meta.completedLoading) return [];

      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        let {limit, offset} = state.meta;
        const condition = offset + limit;

        if (state.list.length >= condition) {
          offset += limit;
        }

        const response = await getIdeas(state.filters, offset, limit);
        const {results, count} = response;

        commit(BULK_UPDATE, results);
        commit(UPDATE_META, {
          loading: false,
          loadingError: false,
          completedLoading: state.list.length >= count,
          offset,
          total: count,
        });

        return results;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async resetAll({commit}: ActionContext<State, RootState>) {
      commit(RESET);
      commit(RESET_FILTERS);
    },
  },
};

export default module;
