import ideas from './modules/ideas';
import {RegistrationStatusModel} from '@/services/Models/RegistrationStatus';
import {ActionContext, Module} from 'vuex';
import {getRegistrationStatus, updateRegistrationStatus} from '@/services/Api/modules/admins';
import {RootState} from '@/store/types';


export interface State {}

const module: Module<State, RootState> = {
  namespaced: true,
  state: {},
  modules: {
    ideas,
  },
  getters: {},
  mutations: {},
  actions: {},
};

export default module;
