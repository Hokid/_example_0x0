export interface Lister {
  callback: () => void;
  vm: any;
}

class Emitter {
  public listeners: Map<string, Lister[]>;
  constructor() {
    this.listeners = new Map();
  }

  public addListener(label: string, callback: () => void, vm: any) {
    if (typeof callback === 'function') {
      this.listeners.has(label) || this.listeners.set(label, []);
      (this.listeners.get(label) as Lister[]).push({callback, vm});
      return true;
    }
    return false;
  }

  public removeListener(label: string, callback: () => void, vm: any) {
    const listeners = this.listeners.get(label);
    let index;

    if (listeners && listeners.length) {
      index = listeners.reduce((i, listener, index) => {
        if (typeof listener.callback === 'function' && listener.callback === callback && listener.vm === vm) {
          i = index;
        }
        return i;
      }, -1);

      if (index > -1) {
        listeners.splice(index, 1);
        this.listeners.set(label, listeners);
        return true;
      }
    }
    return false;
  }

  public emit(label: string, ...args: any[]) {
    const listeners = this.listeners.get(label);

    if (listeners && listeners.length) {
      listeners.forEach((listener) => {
        listener.callback.call(listener.vm, ...args);
      });
      return true;
    }
    return false;
  }
}


export default new Emitter();
