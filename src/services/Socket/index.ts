import Emitter from './Emitter';
import {Store} from 'vuex';

export interface Options {
  format?: string;
  reconnection?: boolean;
  reconnectionDelay?: number;
  passToStoreHandler?: boolean;
  store?: Store<any>;
  storeNamespace?: string;
  reconnectionAttempts?: number;
  protocol?: string;
}

export interface CustomWebSocket extends WebSocket {
  [key: string]: any;
}

export class SocketService {
  public format: string | void;
  public connectionUrl: string;
  public opts: Options;
  public reconnection: boolean;
  public reconnectionDelay: any;
  public reconnectTimeoutId: number;
  public reconnectionCount: number;
  public passToStoreHandler: any;
  public reconnectionAttempts: number;
  public store: Store<any> | void;
  public WebSocket: CustomWebSocket | undefined;

  constructor(connectionUrl: string, opts: Options = {}) {
    this.format = opts.format && opts.format.toLowerCase();
    this.connectionUrl = connectionUrl;
    this.opts = opts;

    this.reconnection = this.opts.reconnection || false;
    this.reconnectionAttempts = this.opts.reconnectionAttempts || Infinity;
    this.reconnectionDelay = this.opts.reconnectionDelay || 1000;
    this.reconnectTimeoutId = 0;
    this.reconnectionCount = 0;

    this.passToStoreHandler = this.opts.passToStoreHandler || false;

    if (opts.store) {
      this.store = opts.store;
    }
  }

  public send(data: any) {
    if (this.WebSocket) {
      this.WebSocket.send(data);
    }
  }

  public sendObject(data: { [key: string]: any }) {
    if (this.format !== 'json') return;

    if (this.WebSocket) {
      this.WebSocket.send(JSON.stringify(data));
    }
  }

  public connect() {
    if (this.opts.reconnection) {
      this.reconnection = true;
    }

    const protocol = this.opts.protocol || '';

    this.WebSocket = (protocol === '' ? new WebSocket(this.connectionUrl) : new WebSocket(this.connectionUrl, protocol)) as CustomWebSocket;

    this.onEvent();

    return this.WebSocket;
  }

  public disconnect() {
    if (this.reconnection) {
      this.reconnection = false;
    }

    (this.WebSocket as CustomWebSocket).close();
  }

  public reconnect() {
    if (this.reconnectionCount <= this.reconnectionAttempts) {
      this.reconnectionCount++;
      clearTimeout(this.reconnectTimeoutId);

      this.reconnectTimeoutId = setTimeout(() => {
        if (!this.reconnection) {
          return;
        }

        if (this.store) {
          this.passToStore('SOCKET_RECONNECT', this.reconnectionCount);
        }

        this.connect();
      }, this.reconnectionDelay);
    } else {
      if (this.store) {
        this.passToStore('SOCKET_RECONNECT_ERROR', true);
      }
    }
  }

  public onEvent() {
    ['onmessage', 'onclose', 'onerror', 'onopen'].forEach((eventType) => {
      (this.WebSocket as CustomWebSocket)[eventType] = (event: any) => {
        Emitter.emit(eventType, event);

        if (this.store) {
          this.passToStore('SOCKET_' + eventType, event);
        }

        if (this.reconnection && eventType === 'onopen') {
          this.reconnectionCount = 0;
        }

        if (this.reconnection && eventType === 'onclose') {
          this.reconnect();
        }
      };
    });
  }

  public passToStore(eventName: string, event: any) {
    if (this.passToStoreHandler) {
      this.passToStoreHandler(eventName, event, this.defaultPassToStore.bind(this));
    } else {
      this.defaultPassToStore(eventName, event);
    }
  }

  public defaultPassToStore(eventName: string, event: any) {
    if (!eventName.startsWith('SOCKET_')) {
      return;
    }
    const method = 'dispatch';
    let target = eventName.toUpperCase();
    let msg = event;
    if (this.format === 'json' && event.data) {
      msg = JSON.parse(event.data);
    }

    if (this.opts.storeNamespace) {
      target = `${this.opts.storeNamespace}${target}`;
    }

    (this.store as { [key: string]: any })[method](target, { message: msg, Socket: this });
  }
}
