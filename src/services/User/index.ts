import Router, {NavigationGuard} from 'vue-router';
import {
  AUTH_STATE_NO_AUTH,
  AUTH_STATE_AUTH_SUCCESS,
  AUTH_STATE_AUTH_PROCESS,
  AUTH_STATE_AUTH_FAIL,
  STORE_AUTH_STATE,
  STORE_RESET,
  STORE_UPDATE,
} from './vuex';
import { PUSH_IDEA } from '@/services/User/vuex/modules/ideas';
import {
  createIdea,
  getAccount,
  getAuthTokenByLoginAndPassword,
  logout,
  registration,
  resetPasswordStep1,
  resetPasswordStep2, updateUser,
} from '@/services/Api/modules/user';
import {Store} from 'vuex';
import EventEmitter = NodeJS.EventEmitter;
import {UserModel, User} from '@/services/Models/User';
import {ErrorModel} from '@/services/Api/models/Error';
import {RegistrationModel} from '@/services/Models/Registration';
import {AuthToken, AuthTokenModel} from '@/services/Models/AuthToken';
import {LoginModel} from '@/services/Models/Login';
import {ResetPasswordStep1Model} from '@/services/Models/ResetPasswordStep1';
import {ResetPasswordStep2Model} from '@/services/Models/ResetPasswordStep2';
import {CreateIdeaModel} from '@/services/Models/CreateIdea';
import {UserUpdate, UserUpdateModel} from '@/services/Models/UserUpdate';
import {VueRouter} from 'vue-router/types/router';
import {SocketService} from '@/services/Socket';
import {IdeaUpdate, IdeaUpdateModel} from '@/services/Models/IdeaUpdate';
import {IdeaInternal} from '@/services/Models/Idea';

class StoreWatcher {
  private Store: Store<any>;
  private User: UserService;
  private NotificationSocket: SocketService;

  constructor(Store: Store<any>, User: UserService, NotificationSocket: SocketService) {
    this.Store = Store;
    this.User = User;
    this.NotificationSocket = NotificationSocket;

    this.run();
  }

  public run() {
    // connect/disconnect to socket by user auth status
    this.Store.watch(
      () => this.Store.getters['user/isAuth'],
      (state: boolean) => {
        if (state) {
          this.NotificationSocket.connect();
        } else {
          this.NotificationSocket.disconnect();
        }
      },
    );

    // login on socket connected
    this.Store.watch(
      (state) => state.user.notifications.socket.isConnected,
      (state: boolean) => {
        if (state) {
          this.NotificationSocket.sendObject(new AuthTokenModel({ token: this.User.token() }));
        }
      },
    );

    // fetch notifications list on login
    this.Store.watch(
      (state) => state.user.notifications.socket.isAuth,
      async (state: boolean) => {
        const id = this.User.id();

        if (state) {
          await this.Store.dispatch(`user/notifications/fetchFullList`, {userId: id});
        }
      },
    );

    // trigger state update depending on notification type
    this.Store.subscribe(
      async (mutation, state) => {
        const id = this.User.id();

        if (mutation.type === 'user/notifications/SOCKET_ONMESSAGE_IDEA_NOTIFICATION') {
          try {
            await this.Store.dispatch(`user/ideas/fetchById`, { userId: id, ideaId: mutation.payload.object_id });
          } catch (e) {
            console.log(e);
          }
        } else if (mutation.type === 'user/notifications/SOCKET_ONMESSAGE_RATING_NOTIFICATION') {
          try {
            await this.Store.dispatch(`user/rating/get`, id);
          } catch (e) {
            console.log(e);
          }
        }
      },
    );
  }
}

class UserService {
  private Store: Store<any>;
  private EventEmitter: EventEmitter;
  private Router: VueRouter;
  private NotificationSocket: SocketService;
  private StoreWatcher: StoreWatcher;

  constructor(StoreInst: Store<any>, Router: VueRouter, EE: EventEmitter, NotificationSocket: SocketService) {
    this.Store = StoreInst;
    this.EventEmitter = EE;
    this.Router = Router;
    this.NotificationSocket = NotificationSocket;
    this.StoreWatcher = new StoreWatcher(StoreInst, this, NotificationSocket);
  }

  public async loginByToken(token: string) {
    const prevToken = this.Store.state.user.token;

    this.Store.commit(`user/${STORE_UPDATE}`, { token });

    try {
      const accountData: UserModel = await getAccount();
      return await this.endLogin(accountData, null);
    } catch (ER) {
      this.Store.commit(`user/${STORE_UPDATE}`, { token: null });
      return await this.endLogin(null, ER);
    }
  }

  public async login(email: string, password: string) {
    this.Store.commit(`user/${STORE_AUTH_STATE}`, AUTH_STATE_AUTH_PROCESS);

    try {
      const tokenModel: AuthToken = await getAuthTokenByLoginAndPassword(
        new LoginModel({email, password}),
      );

      const { token } = tokenModel;

      this.Store.commit(`user/${STORE_UPDATE}`, { token });

      const accountData = await getAccount();

      return await this.endLogin(accountData, null);
    } catch (ER) {
      // this.Store.commit(`user/${STORE_UPDATE}`, { token: null });

      return await this.endLogin(null, ER);
    }
  }

  public async logout(callApi = true) {
    // allow fail
    try {
      if (callApi) {
        await logout();
      }
    } catch (e) {
      console.log(e);
    } finally {
      await this.Store.dispatch('user/logout');
      this.Router.push('/');
    }
  }

  public isAuth() {
    return this.Store.getters['user/isAuth'];
  }

  public isRoleExpert() {
    return this.Store.getters['user/isRoleExpert'];
  }

  public isRoleModerator() {
    return this.Store.getters['user/isRoleModerator'];
  }

  public token() {
    return this.Store.state.user.token;
  }

  public id() {
    return this.Store.state.user.account.id;
  }

  private async endLogin(model: UserModel | null, error: ErrorModel | null) {
    // this.EventClient.emit('services:User:beforeLogin', request);

    if (error) {
      this.Store.commit(`user/${STORE_AUTH_STATE}`, AUTH_STATE_AUTH_FAIL);
      // this.EventClient.emit('services:User:loginFail', error);

      throw error;
    } else if (model) {
      const id = model.id;

      this.Store.commit(`user/${STORE_UPDATE}`, { account: model });

      try {
        await this.Store.dispatch(`user/rating/get`, id);
        await this.Store.dispatch(`user/ratingHistory/fetch`, id);
      } catch (e) {
        this.Store.commit(`user/${STORE_AUTH_STATE}`, AUTH_STATE_AUTH_FAIL);
        // this.EventClient.emit('services:User:loginFail', error);

        throw e;
      }

      this.Store.commit(`user/${STORE_AUTH_STATE}`, AUTH_STATE_AUTH_SUCCESS);

      // this.EventClient.emit('services:User:loginSuccess', model, RS);
      return model;
    }
  }

  public async registration(model: RegistrationModel) {
    const authToken = await registration(model);

    await this.loginByToken(authToken.token);
  }

  public async resetPasswordStep1(model: ResetPasswordStep1Model) {
    return await resetPasswordStep1(model);
  }

  public async resetPasswordStep2(model: ResetPasswordStep2Model) {
    return await resetPasswordStep2(model);
  }

  public async createIdea(model: CreateIdeaModel): Promise<IdeaInternal> {
    if (!this.isAuth()) {
      throw new Error('unauthorized');
    }

    const id = this.Store.state.user.account.id;

    const idea = await createIdea(id, model);

    this.Store.commit(`user/ideas/${PUSH_IDEA}`, idea);

    return idea;
  }

  public async updateIdea(ideaId: number | string, model: IdeaUpdate) {
    if (!this.isAuth()) {
      throw new Error('unauthorized');
    }

    const id = this.Store.state.user.account.id;

    const idea = await this.Store.dispatch(
      `user/ideas/updateById`,
      { userId: id, ideaId, data: model },
    );

    return idea;
  }

  public async fetchIdeaById(ideaId: number | string) {
    if (!this.isAuth()) {
      throw new Error('unauthorized');
    }

    const id = this.Store.state.user.account.id;

    const idea = await this.Store.dispatch(
      `user/ideas/fetchById`,
      {userId: id, ideaId},
    );

    return idea;
  }

  public async createIdeaAndSendOnModeration(model: CreateIdeaModel) {
    if (!this.isAuth()) {
      throw new Error('unauthorized');
    }

    const createdIdea = await this.createIdea(model);
    const updateRequest = new IdeaUpdateModel({ status_id: IdeaUpdateModel.STATUS_MODERATION });
    const updatedIdea = await this.updateIdea(createdIdea.id, updateRequest);

    return updatedIdea;
  }

  public async updateAccount(model: UserUpdateModel) {
    if (!this.isAuth()) {
      throw new Error('unauthorized');
    }

    const id = this.Store.state.user.account.id;

    const data = await updateUser(id, model);

    this.Store.commit(`user/${STORE_UPDATE}`, {account: data});

    return data;
  }
}


function patchRouter(RouterInstance: Router, User: UserService, loginUrl = '/login') {
  const resolveEnd: NavigationGuard = (to, from, next) => {
    if (to.meta.requireAuth === true && !User.isAuth()) {
      next({
        path: loginUrl,
        query: {
          redirect: to.fullPath,
        },
      });
    } else {
      next();
    }
  };

  RouterInstance.beforeEach((to, from, next) => {
    if (!User.isAuth()) {
      if (User.token()) {
        return User.loginByToken(User.token())
          .then(() => {
            next();
          })
          .catch(() => {
            resolveEnd(to, from, next);
          });
      }
    }

    resolveEnd(to, from, next);
  });
}



export {
  UserService,
  patchRouter,
};
