import {ActionContext, Module} from 'vuex';
import {getRating} from '@/services/Api/modules/user';
import {RootState} from '@/store/types';

export const RESET = 'RESET';
export const UPDATE = 'UPDATE';
export const UPDATE_META = 'UPDATE_META';

export interface StateMeta {
    loading: boolean;
    loadingError: boolean;
}

export interface State {
    meta: StateMeta;
    data: any;
}

const module: Module<State, RootState> = {
  namespaced: true,
  state: {
    meta: {
      loading: false,
      loadingError: false,
    },
    data: null,
  },
  mutations: {
    [UPDATE_META](state: State, payload: StateMeta) {
      Object.assign(state.meta, payload);
    },
    [UPDATE](state: State, payload: State) {
      const data = payload.data;

      Object.assign(state, {data});
    },
    [RESET](state: State) {
      state.data = null;
    },
  },
  actions: {
    async get({commit}: ActionContext<State, any>, userId: string) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const data = await getRating(userId);

        commit(UPDATE, {data});
        commit(UPDATE_META, {loading: false, loadingError: false});
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
  },
};

export default module;
