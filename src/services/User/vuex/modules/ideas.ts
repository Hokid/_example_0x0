import {ActionContext, Module} from 'vuex';
import {getIdea, getIdeas, getRating, updateIdea} from '@/services/Api/modules/user';
import {Idea, IdeaInternal} from '@/services/Models/Idea';
import {IdeaUpdate, IdeaUpdateModel} from '@/services/Models/IdeaUpdate';
import {RootState} from '@/store/types';

export const RESET = 'RESET';
export const UPDATE = 'UPDATE';
export const UPDATE_IDEA = 'UPDATE_IDEA';
export const UPDATE_META = 'UPDATE_META';
export const PUSH_IDEA = 'PUSH_IDEA';
export const BULK_UPDATE = 'BULK_UPDATE';

export interface StateMeta {
  loading: boolean;
  loadingError: boolean;
  completedLoading: boolean;
  offset: number;
  limit: number;
  total: number;
}

export interface State {
  meta: StateMeta;
  list: IdeaInternal[];
}

const module: Module<State, RootState> = {
  namespaced: true,
  state: {
    meta: {
      loading: false,
      loadingError: false,
      // list full loaded indicator
      completedLoading: false,
      offset: 0,
      limit: 10,
      total: 0,
    },
    list: [],
  },
  mutations: {
    [UPDATE_META](state: State, payload: StateMeta) {
      Object.assign(state.meta, payload);
    },
    [UPDATE](state: State, payload: State) {
      const list = payload.list;

      Object.assign(state, {list});
    },
    [UPDATE_IDEA](state: State, item: IdeaInternal) {
      const list = state.list;
      const foundIndex = list.findIndex((idea) => idea.id === item.id);

      if (foundIndex >= 0) {
        list.splice(foundIndex, 1, item);
      } else {
        list.unshift(item);
      }
    },
    [PUSH_IDEA](state: State, payload: IdeaInternal) {
      state.list.unshift(payload);
    },
    [BULK_UPDATE](state: State, items: IdeaInternal[]) {
      for (const item of items) {
        const list = state.list;
        const foundIndex = list.findIndex((idea) => idea.id === item.id);

        if (foundIndex >= 0) {
          list.splice(foundIndex, 1, item);
        } else {
          list.push(item);
        }
      }
    },
    [RESET](state: State) {
      state.meta.loading = false;
      state.meta.loadingError = false;
      state.meta.completedLoading = false;
      state.meta.offset = 0;
      state.meta.limit = 10;
      state.meta.total = 0;
      state.list = [];
    },
  },
  actions: {
    async fetchAll({commit, state}: ActionContext<State, RootState>, userId: string) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const {count, results} = await getIdeas(userId);

        commit(UPDATE, {list: results});
        commit(UPDATE_META, {
          loading: false,
          loadingError: false,
          completedLoading: true,
          offset: count - state.meta.limit,
          total: count,
        });

        return results;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async fetchById(
      {commit}: ActionContext<State, RootState>,
      {userId, ideaId}: { userId: string, ideaId: string | number },
    ) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const idea = await getIdea(userId, ideaId);

        commit(UPDATE_IDEA, idea);
        commit(UPDATE_META, {loading: false, loadingError: false});

        return idea;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async updateById(
      {commit}: ActionContext<State, RootState>,
      {userId, ideaId, data}: { userId: string, ideaId: number | string, data: IdeaUpdate },
    ) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const idea = await updateIdea(userId, ideaId, new IdeaUpdateModel(data));

        commit(UPDATE_IDEA, idea);
        commit(UPDATE_META, {loading: false, loadingError: false});

        return idea;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async fetchNext({commit, state}: ActionContext<State, RootState>, userId: string | number) {
      if (state.meta.completedLoading) return [];

      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        let {limit, offset} = state.meta;
        const condition = offset + limit;

        if (state.list.length >= condition) {
          offset += limit;
        }

        const response = await getIdeas(userId, offset, limit);
        const {results, count} = response;

        commit(BULK_UPDATE, results);
        commit(UPDATE_META, {
          loading: false,
          loadingError: false,
          completedLoading: state.list.length >= count,
          offset,
          total: count,
        });

        return results;
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async resetAll({commit}: ActionContext<State, RootState>) {
      commit(RESET);
    },
  },
};

export default module;
