import {ActionContext, Module} from 'vuex';
import {AuthSocketStatus, AuthSocketStatusModel} from '@/services/Models/AuthSocketStatus';
import {IdeaNotificationSocket} from '@/services/Models/IdeaNotificationSocket';
import {NotificationMessageStatus, NotificationMessageStatusModel} from '@/services/Models/NotificationMessageStatus';
import {RatingNotificationSocket} from '@/services/Models/RatingNotificationSocket';
import {SocketService} from '@/services/Socket';
import {getNotifications, markNotificationAsViewed} from '@/services/Api/modules/user';
import {IdeaNotification} from '@/services/Models/IdeaNotification';
import {RatingNotification} from '@/services/Models/RatingNotification';
import {RootState} from '@/store/types';

export const RESET = 'RESET';
export const UPDATE = 'UPDATE';
export const UPDATE_META = 'UPDATE_META';
export const DELETE_NOTIFICATION = 'DELETE_NOTIFICATION';

export type IncomingMessage = AuthSocketStatus | IdeaNotificationSocket | RatingNotificationSocket;
export type Notification = IdeaNotification | RatingNotification;

export interface SocketState {
  isConnected: boolean;
  isAuth: boolean;
  isAuthError: boolean;
  error: any;
  lastMessage: IncomingMessage | null;
  reconnectionCount: number;
  reconnectError: boolean;
}

export interface StateMeta {
  loading: boolean;
  loadingError: boolean;
}

export interface State {
  meta: StateMeta;
  socket: SocketState;
  list: Notification[];
}

const module: Module<State, RootState> = {
  namespaced: true,
  state: {
    meta: {
      loading: false,
      loadingError: false,
    },
    socket: {
      isConnected: false,
      isAuth: false,
      isAuthError: false,
      error: null,
      lastMessage: null,
      reconnectionCount: 0,
      reconnectError: false,
    },
    list: [
      // { message: { id: '1', title: 'test', content: 'test' }, type: 'idea', object_id: '1' },
      // { message: { id: '1', title: 'test', content: 'test' }, type: 'rating' },
    ],
  },
  mutations: {
    [UPDATE_META](state: State, payload: StateMeta) {
      Object.assign(state.meta, payload);
    },
    [UPDATE](state: State, payload: State) {
      const list = payload.list;

      Object.assign(state, {list});
    },
    [RESET](state: State) {
      state.list = [];
    },
    [DELETE_NOTIFICATION](state: State, id: string) {
      const foundIdx = state.list.findIndex((item) => item.id === id);

      if (foundIdx >= 0) {
        state.list.splice(foundIdx, 1);
      }
    },
    SOCKET_ONOPEN(state: State, event: any) {
      state.socket.isConnected = true;
    },
    SOCKET_ONCLOSE(state: State, event: any) {
      state.socket.isConnected = false;
    },
    SOCKET_ONERROR(state: State, event: any) {
      state.socket.error = event;
    },
    // default handler called for all methods
    SOCKET_ONMESSAGE(state: State, message: IncomingMessage) {
      state.socket.lastMessage = message;
    },
    SOCKET_ONMESSAGE_AUTH_STATUS(state: State, message: AuthSocketStatus) {
      if (message.status === AuthSocketStatusModel.STATUS_SUCCESS) {
        state.socket.isAuth = true;
        state.socket.isAuthError = false;
      } else {
        state.socket.isAuth = false;
        state.socket.isAuthError = true;
      }
    },
    SOCKET_ONMESSAGE_IDEA_NOTIFICATION(state: State, message: IdeaNotification) {
      state.list.push(message);
    },
    SOCKET_ONMESSAGE_RATING_NOTIFICATION(state: State, message: RatingNotification) {
      state.list.push(message);
    },
    // mutations for reconnect methods
    SOCKET_RECONNECT(state: State, count: number) {
      state.socket.reconnectionCount = count;
    },
    SOCKET_RECONNECT_ERROR(state: State) {
      state.socket.reconnectError = true;
    },
  },
  actions: {
    async fetchFullList({commit}: ActionContext<State, any>, {userId}: { userId: string }) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const list = await getNotifications(userId);

        commit(UPDATE, {list});
        commit(UPDATE_META, {loading: false, loadingError: false});
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
    async markAsViewed({commit, rootState}: ActionContext<State, any>, {userId, id}: { userId: string, id: string }) {
      commit(DELETE_NOTIFICATION, id);

      try {
        await markNotificationAsViewed(userId, id);
      } catch (e) {
        console.log(e);
      }
    },
    async confirmDelivery({commit}: ActionContext<State, any>, {id, Socket}: { [key: string]: any, id: string, Socket: SocketService }) {
      Socket.sendObject(new NotificationMessageStatusModel({
        message: {
          id,
          status: NotificationMessageStatusModel.STATUS_SUCCESS,
        },
      }));
    },
    SOCKET_ONOPEN({commit}: ActionContext<State, any>, event: any) {
      commit('SOCKET_ONOPEN', event);
    },
    SOCKET_ONCLOSE({commit}: ActionContext<State, any>, event: any) {
      commit('SOCKET_ONCLOSE', event);
    },
    SOCKET_ONERROR({commit}: ActionContext<State, any>, event: any) {
      commit('SOCKET_ONERROR', event);
    },
    // default handler called for all methods
    async SOCKET_ONMESSAGE({commit, dispatch}: ActionContext<State, any>, {message, Socket}: { message: IncomingMessage, Socket: SocketService }) {
      const confirm = async (id: string) => {
        try {
          await dispatch('confirmDelivery', {id, Socket});
        } catch (e) {
          console.log(e);
        }
      };

      commit('SOCKET_ONMESSAGE', message);

      if (message.hasOwnProperty('status')) {
        const msg = message as AuthSocketStatus;

        commit('SOCKET_ONMESSAGE_AUTH_STATUS', msg);
      } else if (message.hasOwnProperty('message')) {

        const messageData = (message as IdeaNotificationSocket | RatingNotificationSocket).message;

        if (messageData.type === 'idea') {
          const msg = messageData as IdeaNotification;

          commit('SOCKET_ONMESSAGE_IDEA_NOTIFICATION', msg);

          await confirm(msg.id);
        } else if (messageData.type === 'rating') {
          const msg = messageData as RatingNotification;

          commit('SOCKET_ONMESSAGE_RATING_NOTIFICATION', msg);

          await confirm(msg.id);
        }
      }
    },
    // mutations for reconnect methods
    SOCKET_RECONNECT({commit}: ActionContext<State, any>, count: number) {
      commit('SOCKET_RECONNECT', count);
    },
    SOCKET_RECONNECT_ERROR({commit}: ActionContext<State, any>) {
      commit('SOCKET_RECONNECT_ERROR');
    },
  },
};

export default module;
