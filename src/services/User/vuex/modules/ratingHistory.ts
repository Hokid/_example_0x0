import {ActionContext, Module} from 'vuex';
import {getRatingHistory} from '@/services/Api/modules/user';
import {RatingHistoryItem} from '@/services/Models/RatingHistoryItem';
import {RootState} from '@/store/types';

export const RESET = 'RESET';
export const UPDATE = 'UPDATE';
export const UPDATE_META = 'UPDATE_META';

export interface StateMeta {
    loading: boolean;
    loadingError: boolean;
}

export interface State {
    meta: StateMeta;
    list: RatingHistoryItem[];
}

const module: Module<State, RootState> = {
  namespaced: true,
  state: {
    meta: {
      loading: false,
      loadingError: false,
    },
    list: [],
  },
  mutations: {
    [UPDATE_META](state: State, payload: StateMeta) {
      Object.assign(state.meta, payload);
    },
    [UPDATE](state: State, payload: State) {
      const list = payload.list;

      Object.assign(state, {list});
    },
    [RESET](state: State) {
      state.list = [];
    },
  },
  actions: {
    async fetch({commit}: ActionContext<State, any>, userId: string) {
      commit(UPDATE_META, {loading: true, loadingError: false});

      try {
        const list = await getRatingHistory(userId);

        commit(UPDATE, {list});
        commit(UPDATE_META, {loading: false, loadingError: false});
      } catch (e) {
        commit(UPDATE_META, {loading: false, loadingError: true});

        throw e;
      }
    },
  },
};

export default module;
