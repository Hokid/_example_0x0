import cloneDeep from 'lodash/cloneDeep';
import {ActionContext, Module} from 'vuex';
import rating, {RESET as RATING_RESET, State as RatingState} from './modules/rating';
import ideas, {RESET as IDEAS_RESET, State as IdeasState} from './modules/ideas';
import ratingHistory, {RESET as RATING_HISTORY_RESET, State as RatingHistoryState} from './modules/ratingHistory';
import notifications, {RESET as NOTIFICATIONS_RESET, State as NotificationState} from './modules/notifications';
import {User, UserRole} from '@/services/Models/User';
import {RootState} from '@/store/types';


export const AUTH_STATE_NO_AUTH = 0;
export const AUTH_STATE_AUTH_PROCESS = 1;
export const AUTH_STATE_AUTH_SUCCESS = 2;
export const AUTH_STATE_AUTH_FAIL = 3;

export const STORE_AUTH_STATE = 'STORE_AUTH_STATE';
export const STORE_UPDATE = 'STORE_UPDATE';
export const STORE_RESET = 'STORE_RESET';

export type AUTH_STATE = 0 | 1 | 2 | 3;

export interface StateMeta {
  authState: AUTH_STATE;
}

export interface StateAccount extends User {}

export interface State {
  meta: StateMeta;
  account: StateAccount;
  token: string;
}

type Action = ActionContext<State, any>;

const $state = {
  meta: {
    authState: AUTH_STATE_NO_AUTH,
  },
  account: {
    id: '',
    first_name: '',
    last_name: '',
    middle_name: '',
    email: '',
    company_id: 0,
    job_id: 0,
    phone: '',
    photo: '',
    work_experience: 0,
    role: [],
    rating: 0,
    ideas_count: 0,
    significantRole: '',
    significantRoleText: ''
  },
  token: '',
};

const module: Module<State, RootState> = {
  namespaced: true,
  state: cloneDeep($state) as State,
  modules: {
    rating,
    ideas,
    ratingHistory,
    notifications,
  },
  getters: {
    isAuth(state: State) {
      return state.meta.authState === AUTH_STATE_AUTH_SUCCESS;
    },
    isRoleExpert(state: State) {
      return state.account.role ? state.account.role.includes(UserRole.expert) : false;
    },
    isRoleModerator(state: State) {
      return state.account.role ? state.account.role.includes(UserRole.moderator) : false;
    },
    isRoleAdministrator(state: State) {
      return state.account.role ? state.account.role.includes(UserRole.administrator) : false;
    },
    /* Вернуть список важных ролей у пользователя (которые влияют на интерфейс) Администратор, модератор, эксперт */
    importantRoles(state: State) {
      const roles = state.account.role;
      return roles ? roles.filter((obj: UserRole) =>
        obj === UserRole.expert ||
        obj === UserRole.moderator ||
        obj === UserRole.administrator) : [];
    },
  },
  mutations: {
    [STORE_AUTH_STATE](state: State, stateSign: AUTH_STATE) {
      state.meta.authState = stateSign;
    },
    [STORE_UPDATE](state: State, payload: State) {
      Object.assign(state, payload, { meta: state.meta });
    },
    [STORE_RESET](state: State) {
      Object.assign(state, cloneDeep($state));
    },
  },
  actions: {
    logout({ commit }: Action) {
      commit(`rating/${RATING_RESET}`);
      commit(`ideas/${IDEAS_RESET}`);
      commit(`ratingHistory/${RATING_HISTORY_RESET}`);
      commit(`notifications/${NOTIFICATIONS_RESET}`);
      commit(STORE_RESET);
    },
  },
};

export default module;
