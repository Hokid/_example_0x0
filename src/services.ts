import Vue from 'vue';
import GEvents from '@hokid/webapp-service-global-events';
import store from './store';
import router, { addMainPageRoute } from './router';
import { patchRouter, UserService } from '@/services/User';
import {
  emitEventsInterceptor,
  injectBearerAuthInterceptor,
  injectLogoutOnTokenExpireInterceptor,
} from '@/services/Api/interceptors';
import Auth from '@/services/Api/Auth';
import {SocketService} from '@/services/Socket/index';

const NotificationSocket = new SocketService(
  process.env.VUE_APP_SOCKET_HOST + '/notifications',
  {
    reconnection: true,
    format: 'json',
    reconnectionAttempts: 10,
    store,
    storeNamespace: 'user/notifications/',
  },
);

const User = new UserService(store, router, GEvents, NotificationSocket);

addMainPageRoute(User, router);
patchRouter(router, User);
injectBearerAuthInterceptor(Auth, () => User.token());
injectLogoutOnTokenExpireInterceptor(Auth, User);

Vue.use({
  install($Vue, options) {
    $Vue.prototype.$services = {
      // inject services here
      GlobalEvents: GEvents,
      User,
    };
  },
});

if (process.env.NODE_ENV === 'development') {
  (window as any).__DEBUG = {
    GEvents,
    store,
    router,
    User,
  };
}

export {
  GEvents,
  store,
  router,
  User,
};
