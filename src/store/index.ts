import Vue from 'vue';
import Vuex, {ActionContext} from 'vuex';
import PersistedStatePlugin from 'vuex-persistedstate';
import user from '@/services/User/vuex';
import companies from './modules/companies';
import jobs from './modules/jobs';
import ideaCategories from './modules/ideaCategories';
import experts from './modules/experts';
import privileges from './modules/privileges';
import moderators from '@/services/Moderators/vuex';
import expertise from '@/services/Expertise/vuex';
import admins from '@/services/Admins/vuex';
import {getRegistrationStatus, updateRegistrationStatus} from '@/services/Api/modules/admins';
import {RegistrationStatusInternal, RegistrationStatusModel} from '@/services/Models/RegistrationStatus';
import { RootState } from '@/store/types';

Vue.use(Vuex);

export const UPDATE_REGISTRATION_STATUS = 'UPDATE_REGISTRATION_STATUS';

const store = {
  state: {
    isRegistrationAvailable: false,
  },
  mutations: {
    [UPDATE_REGISTRATION_STATUS](state: RootState, payload: boolean) {
      state.isRegistrationAvailable = payload;
    },
  },
  getters: {},
  actions: {
    async fetchRegistrationStatus({commit}: ActionContext<RootState, RootState>) {
      const status = await getRegistrationStatus();

      commit(UPDATE_REGISTRATION_STATUS, status.available);

      return status.available;
    },
    async updateRegistrationStatus({commit}: ActionContext<RootState, RootState>, payload: RegistrationStatusInternal) {
      const status = await updateRegistrationStatus(payload);

      commit(UPDATE_REGISTRATION_STATUS, status.available);

      return status.available;
    },
  },
  modules: {
    user,
    companies,
    jobs,
    ideaCategories,
    experts,
    privileges,
    moderators,
    expertise,
    admins,
  },
  plugins: [
    PersistedStatePlugin({
      paths: [
        'user.token',
        'moderators.ideas.filters',
        'expertise.ideas.filters',
        'admins.ideas.filters',
      ],
    }),
  ],
};

export default new Vuex.Store<RootState>(store);
