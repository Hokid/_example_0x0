import Vue from 'vue';
import Router from 'vue-router';
import InnerPagesTemplate from '@/views/templates/InnerPagesDefault.vue';
import InnerPagesWithFooterTemplate from '@/views/templates/InnerPagesWithFooter.vue';
import {UserService} from '@/services/User';

Vue.use(Router);

export function addMainPageRoute(User: UserService, RouterInst: Router) {
  RouterInst.addRoutes([
    {
      path: '/',
      beforeEnter(from, to, next) {
        if (User.isAuth()) {
          if (User.isRoleExpert()) {
            next('/expertise');
          } else if (User.isRoleModerator()) {
            next('/moderation');
          } else {
            next('/my-ideas');
          }
        }
      },
    },
  ]);
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: InnerPagesTemplate,
      children: [
        {
          path: '/',
          name: 'Home',
          meta: {
            requireAuth: false,
          },
          async component() {
            return await import('@/views/pages/Home.vue');
          },
        },
      ],
    },
    {
      path: '/login',
      component: InnerPagesTemplate,
      children: [
        {
          path: '/',
          name: 'Login',
          meta: {
            requireAuth: false,
          },
          async component() {
            return await import('@/views/pages/Login.vue');
          },
        },
      ],
    },
    {
      path: '/registration',
      component: InnerPagesTemplate,
      children: [
        {
          path: '/',
          name: 'Registration',
          meta: {
            requireAuth: false,
          },
          async component() {
            return await import('@/views/pages/Registration.vue');
          },
        },
      ],
    },
    {
      path: '/reset-password',
      component: InnerPagesTemplate,
      children: [
        {
          path: '/',
          name: 'ResetPassword',
          meta: {
            requireAuth: false,
          },
          async component() {
            return await import('@/views/pages/ResetPassword.vue');
          },
        },
      ],
    },
    {
      path: '/profile',
      component: InnerPagesWithFooterTemplate,
      children: [
        {
          path: '/',
          name: 'Profile',
          meta: {
            requireAuth: true,
          },
          async component() {
            return await import('@/views/pages/Profile.vue');
          },
        },
      ],
    },
    {
      path: '/create-idea',
      component: InnerPagesWithFooterTemplate,
      children: [
        {
          path: '/',
          name: 'CreateIdea',
          meta: {
            requireAuth: true,
          },
          async component() {
            return await import('@/views/pages/CreateIdea.vue');
          },
        },
      ],
    },
    {
      path: '/edit-idea/:id',
      component: InnerPagesWithFooterTemplate,
      children: [
        {
          path: '/',
          name: 'EditIdea',
          meta: {
            requireAuth: true,
          },
          async component() {
            return await import('@/views/pages/EditIdea.vue');
          },
        },
      ],
    },
    {
      path: '/edit-idea-admin/:id',
      component: InnerPagesWithFooterTemplate,
      children: [
        {
          path: '/',
          name: 'EditIdeaAdmin',
          meta: {
            requireAuth: true,
          },
          async component() {
            return await import('@/views/pages/EditIdeaAdmin.vue');
          },
        },
      ],
    },
    {
      path: '/expertise/:id',
      component: InnerPagesWithFooterTemplate,
      children: [
        {
          path: '/',
          name: 'IdeaExpertise',
          meta: {
            requireAuth: true,
          },
          async component() {
            return await import('@/views/pages/IdeaExpertise.vue');
          },
        },
      ],
    },
    {
      path: '/my-rating',
      component: InnerPagesWithFooterTemplate,
      children: [
        {
          path: '/',
          name: 'MyRating',
          meta: {
            requireAuth: true,
          },
          async component() {
            return await import('@/views/pages/MyRating.vue');
          },
        },
      ],
    },
    {
      path: '/benefits',
      component: InnerPagesWithFooterTemplate,
      children: [
        {
          path: '/',
          name: 'Benefits',
          meta: {
            requireAuth: true,
          },
          async component() {
            return await import('@/views/pages/Benefits.vue');
          },
        },
      ],
    },
    {
      path: '/notifications',
      component: InnerPagesWithFooterTemplate,
      children: [
        {
          path: '/',
          name: 'Notifications',
          meta: {
            requireAuth: true,
          },
          async component() {
            return await import('@/views/pages/Notifications.vue');
          },
        },
      ],
    },
    {
      path: '/error',
      component: InnerPagesTemplate,
      children: [
        {
          path: '/',
          name: 'ErrorPage',
          meta: {
            // meta
          },
          async component() {
            return await import('@/views/pages/Error.vue');
          },
        },
      ],
    },
    {
      path: '/not-found',
      component: InnerPagesTemplate,
      children: [
        {
          path: '/',
          name: 'NotFoundErrorPage',
          meta: {
            isPageNotFound: true,
          },
          async component() {
            return await import('@/views/pages/NotFoundError.vue');
          },
        },
      ],
    },
    {
      path: '*',
      redirect: '/not-found',
    },
  ],
});
