import {RawLocation} from 'vue-router';
import {Vue} from 'vue/types/vue';

declare type RouteNextCallback = (to?: RawLocation | false | ((vm: Vue) => any) | void) => void;
