import {Component, Vue, Watch} from 'vue-property-decorator';

@Component
export default class GlobalErrorsMixin extends Vue {
  public globalError = '';
  public globalErrorShow = false;

  @Watch('globalError', {immediate: true})
  public onGlobalErrorChange(value: string) {
    this.globalErrorShow = !!value;
  }

  @Watch('globalErrorShow', {immediate: true})
  public onGlobalErrorShowChange(value: boolean) {
    if (!value && this.globalError) {
      this.globalError = '';
    }
  }

  resetGlobalError() {
    this.globalError = '';
  }
}
