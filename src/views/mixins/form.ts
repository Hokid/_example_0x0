import {Component, Vue} from 'vue-property-decorator';
import cloneDeep from 'lodash/cloneDeep';


export interface FieldError {
  flag: null | boolean;
  error: string;
};

export type FieldsErrors = {
  [key: string]: FieldError;
}

function generateFieldsErrors<T>(fields: T): FieldsErrors {
  return Object.keys(fields)
    .reduce((result: FieldsErrors, key: string) => {
      result[key] = {
        flag: null,
        error: ''
      };

      return result;
    }, {}) ;
}

export function generateFormMixin<T, K extends keyof T>(name: string, fields: T) {
  if (name.length === 0 && Object.keys(fields).length === 0) throw new TypeError('fill arguments correctly');

  const capName = name[0].toLocaleUpperCase() + name.slice(1);
  const dataErrorsKey = `${name}Errors`;
  const resetMethodErrorsKey = `reset${capName}Errors`;
  const resetMethodKey = `reset${capName}`;

  @Component({
    // because typescript not allow(why?) set computed prop name other than a simple literal
    data() {
      return {
        [name]: cloneDeep(fields),
        [dataErrorsKey]: generateFieldsErrors(fields)
      };
    }
  })
  class FormMixin extends Vue {
    // but... this work
    [resetMethodErrorsKey](): void {
      (this as { [key: string]: any })[dataErrorsKey] = generateFieldsErrors(fields);
    }

    [resetMethodKey](): void {
      (this as { [key: string]: any })[name] = cloneDeep(fields);
    }
  }

  return FormMixin;
}
