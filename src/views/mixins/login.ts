import {Component, Vue, Watch, Mixins} from 'vue-property-decorator';
import {LoginModel} from '@/services/Models/Login';
import {UserDataErrorsItem} from '@/views/mixins/user-data';
import GlobalErrorsMixin from '@/views/mixins/global-errors';

export interface LoginForm {
  login: string;
  password: string;
}

export interface LoginErrorsItem {
  flag: boolean | null;
  errorText: string;
}

export interface LoginErrors {
  [key: string]: LoginErrorsItem;
  login: LoginErrorsItem;
  password: LoginErrorsItem;
}

@Component
export default class LoginMixin extends Mixins(GlobalErrorsMixin) {
  public loginForm: LoginForm = {
    login: '',
    password: '',
  };

  public loginErrors: LoginErrors = {
    login: {
      flag: null,
      errorText: '',
    },
    password: {
      flag: null,
      errorText: '',
    },
  };

  public resetLoginErrors() {
    for (const key in this.loginForm) {
      const item = this.loginErrors[key];

      item.flag = null;
      item.errorText = '';
    }
  }

  public setLoginErrorsError(fieldName: string, error: string) {
    const item = this.loginErrors[fieldName];

    if (item) {
      item.flag = false;
      item.errorText = error;
    }
  }

  public async tryLogin() {
    this.resetLoginErrors();

    try {
      await this.$services.User.login(this.loginForm.login, this.loginForm.password);

      this.$router.push('/');
    } catch (e) {
      if (e.message) {
        this.globalError = e.message;
      }

      if (e.additional) {
        for (const field in e.additional) {
          const errors = e.additional[field];
          let normilized = field;

          switch (field) {
            case 'email':
              normilized = 'login';
              break;
          }

          this.setLoginErrorsError(normilized, errors);
        }
      }
    }
  }
}
