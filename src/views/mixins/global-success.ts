import {Component, Vue, Watch} from 'vue-property-decorator';

@Component
export default class GlobalSuccessMixin extends Vue {
  public globalSuccess = '';
  public globalSuccessShow = false;

  @Watch('globalSuccess', {immediate: true})
  public onGlobalSuccessChange(value: string) {
    this.globalSuccessShow = !!value;
  }

  @Watch('globalSuccessShow', {immediate: true})
  public onGlobalSuccessShowChange(value: boolean) {
    if (!value && this.globalSuccess) {
      this.globalSuccess = '';
    }
  }

  resetGlobalSuccess() {
    this.globalSuccess = '';
  }
}
