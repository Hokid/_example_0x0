import {Component, Vue} from 'vue-property-decorator';
import {UserRole} from '@/services/Models/User';

export interface UserDataForm {
  avatarUrl: string | null;
  lastName: string;
  firstName: string;
  middleName: string;
  email: string;
  phone: string;
  companyId: string;
  jobId: string;
  workExperience: string;
  role?: UserRole;
}

export interface UserDataErrorsItem {
  flag: boolean | null;
  errorText: string;
}

export interface UserDataErrors {
  [key: string]: UserDataErrorsItem;
  avatarUrl: UserDataErrorsItem;
  lastName: UserDataErrorsItem;
  firstName: UserDataErrorsItem;
  middleName: UserDataErrorsItem;
  email: UserDataErrorsItem;
  phone: UserDataErrorsItem;
  companyId: UserDataErrorsItem;
  jobId: UserDataErrorsItem;
  workExperience: UserDataErrorsItem;
  role: UserDataErrorsItem;
}

@Component
export default class UserData extends Vue {
  public userDataForm: UserDataForm = {
    avatarUrl: '',
    lastName: '',
    firstName: '',
    middleName: '',
    email: '',
    phone: '',
    companyId: '',
    jobId: '',
    workExperience: '',
    role: UserRole.user,
  };

  public userDataErrors: UserDataErrors = {
    avatarUrl: {
      flag: null,
      errorText: '',
    },
    lastName: {
      flag: null,
      errorText: '',
    },
    firstName: {
      flag: null,
      errorText: '',
    },
    middleName: {
      flag: null,
      errorText: '',
    },
    email: {
      flag: null,
      errorText: '',
    },
    phone: {
      flag: null,
      errorText: '',
    },
    companyId: {
      flag: null,
      errorText: '',
    },
    jobId: {
      flag: null,
      errorText: '',
    },
    workExperience: {
      flag: null,
      errorText: '',
    },
    role: {
      flag: null,
      errorText: '',
    },
  };

  public resetUserDataErrors() {
    for (const key in this.userDataErrors) {
      const item = this.userDataErrors[key];

      item.flag = null;
      item.errorText = '';
    }
  }

  public setUserDataError(fieldName: string, error: string) {
    const item = this.userDataErrors[fieldName];

    if (item) {
      item.flag = false;
      item.errorText = error;
    }
  }
}
