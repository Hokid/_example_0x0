import {Component, Vue, Prop, Emit} from 'vue-property-decorator';
import nanoid from 'nanoid';

@Component({
  inheritAttrs: false,
})
export default class CommonMixin extends Vue {
  @Prop({default: ''})
  public value!: any;

  @Prop(String)
  public label!: string;

  @Prop(String)
  public text!: string;

  @Prop(String)
  public size!: string;

  public id: string = nanoid();

  get classes() {
    return {
      [`form-control-${this.size}`]: !!this.size,
    };
  }

  get filteredListeners() {
    if (this.$listeners) {
      const copy = Object.assign({}, this.$listeners);

      delete copy.input;

      return copy;
    }

    return {};
  }
}
