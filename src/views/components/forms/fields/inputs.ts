import {Component, Vue, Prop, Emit} from 'vue-property-decorator';
import nanoid from 'nanoid';

@Component
export default class InputsMixin extends Vue {
  public showPassword: boolean = false;

  get internalType() {
    if (this.showPassword) {
      return 'text';
    } else if (this.$attrs.type === 'search') {
      return 'text';
    }

    return this.$attrs.type || 'text';
  }

  get hasAction() {
    return this.$attrs.type === 'password';
  }

  get isSearchType() {
    return this.$attrs.type === 'search';
  }

  public onTogglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }
}
