import {Component, Vue, Prop, Emit} from 'vue-property-decorator';

@Component
export default class ValidationMixin extends Vue {
  @Prop(String)
  public validText!: string;

  @Prop(String)
  public invalidText!: string;

  @Prop(Boolean)
  public requireFlag!: boolean;

  @Prop({default: null, type: [Boolean]})
  public validFlag!: boolean | null;

  get hasValidityFlag() {
    return typeof this.validFlag === 'boolean';
  }

  get validationClasses() {
    return {
      ['is-valid']: this.hasValidityFlag && this.validFlag,
      ['is-invalid']: this.hasValidityFlag && !this.validFlag,
    };
  }
}
