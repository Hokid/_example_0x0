import {Component, Vue, Prop } from 'vue-property-decorator';

@Component({
  inheritAttrs: false,
})
export default class CommonIconMixin extends Vue {
  @Prop({default: '20'})
  public width!: string | number;

  @Prop({default: '20'})
  public height!: string | number;

  @Prop({default: '#000000'})
  public color!: string;
}
