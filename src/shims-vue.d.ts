declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module 'vue-loaders';
declare module 'vuex-persistedstate';
declare module 'bootstrap-vue/es/components';

declare module 'vue-cropperjs';
declare module 'stf-vue-select';
declare module 'vue-star-rating';
