import Vue from 'vue';
import VueMeta from 'vue-meta';
import '@/services/Modality/index.js';
import App from './App.vue';
import {
  store,
  router,
} from './services';
import './registerServiceWorker';
import {
  Modal,
  FormFile,
  Dropdown,
  FormRadio,
  Pagination,
  Table,
} from 'bootstrap-vue/es/components';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import {StfSelect, StfSelectOption} from 'stf-vue-select';
import 'stf-vue-select/dist/lib/stf-vue-select.min.css';
import { Component } from 'vue-property-decorator';

// Register the router hooks with their names
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate',
]);

Vue.use(Modal);
Vue.use(FormFile);
Vue.use(Dropdown);
Vue.use(FormRadio);
Vue.use(Pagination);
Vue.use(Table);

Vue.component('stf-select-option', StfSelectOption);
Vue.component('stf-select', StfSelect);

Vue.config.productionTip = false;

Vue.use(VueMeta);

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount('#app');
