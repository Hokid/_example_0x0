import {UserService} from '@/services/User';
import GlobalEvents from '@hokid/webapp-service-global-events';

export interface $style {
  [key: string]: string | void;
}

export interface Services {
  User: UserService;
  GlobalEvents: typeof GlobalEvents;
}

declare module 'vue/types/vue' {
  interface Vue {
    $services: Services;
    $style: $style;
  }
}

declare module 'vue/types/options' {
  interface RenderContext {
    $style: $style;
  }
}

