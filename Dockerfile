FROM node:carbon

ENV PORT=443
ENV PROXY_URL=/api

ARG PORT=$PORT

WORKDIR /usr/src/app

RUN npm install npm@latest -g
COPY package*.json ./
RUN npm -v
RUN node -v
RUN npm ci

COPY server/package*.json ./server/
RUN cd server/ && npm ci && cd ../

COPY . .

RUN npm run build:modern

EXPOSE $PORT

CMD [ "npm", "run", "docker-server" ]


